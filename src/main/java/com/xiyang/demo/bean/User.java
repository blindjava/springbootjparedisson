package com.xiyang.demo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;

/**
 * @Description  
 * @Author  linmengmeng
 * @Date 2021-04-12 14:44:27 
 */

@Entity
@Table ( name ="user" )
public class User  implements Serializable {

	private static final long serialVersionUID =  5448703750710871645L;

	@Id
   	@Column(name = "id" )
	private Long id;

   	@Column(name = "name" )
	private String name;

   	@Column(name = "age" )
	private Long age;

   	@Column(name = "password" )
	private Integer password;

   	@Column(name = "datetime" )
	private Long datetime;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return this.age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public Integer getPassword() {
		return this.password;
	}

	public void setPassword(Integer password) {
		this.password = password;
	}

	public Long getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		return "{" +
					"id='" + id + '\'' +
					"name='" + name + '\'' +
					"age='" + age + '\'' +
					"password='" + password + '\'' +
					"datetime='" + datetime + '\'' +
				'}';
	}

}
