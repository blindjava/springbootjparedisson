package com.xiyang.demo.service.impl;

import com.xiyang.demo.bean.User;
import com.xiyang.demo.dao.UserRepository;
import com.xiyang.demo.service.LoginService;
import com.xiyang.demo.service.MyAction;
import org.redisson.Redisson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;
    User user;

    @Override
    @MyAction(name = "测试")
    public String login(String name
            , Integer password) {
        Optional<List<User>> allByNameAndpAndPassword = userRepository.findAllByNameAndPassword(name,password);
        if (allByNameAndpAndPassword.isPresent()) {
            user = allByNameAndpAndPassword.get().get(0);
        }
        System.out.println(user);
        return "success";
    }

}