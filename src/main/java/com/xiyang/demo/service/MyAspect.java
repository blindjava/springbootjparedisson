package com.xiyang.demo.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.annotation.Target;
@Aspect
@Component
public class MyAspect {
 @Pointcut("@annotation(com.xiyang.demo.service.MyAction)")
    public   void pointcut(){
     System.out.println("切点方法");
 }
  @Around("pointcut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
      String name = joinPoint.getTarget().getClass().getName();
      System.out.println("环绕增强"+name);
      joinPoint.proceed();
      System.out.println("环绕增强"+name);
  }

}
